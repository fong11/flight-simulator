CS 418 MP2 README

by:
Michael Fong
fong11

To open the project you'll need Microsoft Visual Studio 2013. 
Open the mp2.sln file within the Source folder to run the project. 

Controls:
- : lower the sea level
+ or = : raise the sea level
f : increase the size of the polygons
c : decrease the size of the polygons
ESC : quit

<- : roll to the left
-> : roll to the right
up arrow : pitch up
down arrow: pitch down

Youtube:
http://youtu.be/vCC0auzjkjM
